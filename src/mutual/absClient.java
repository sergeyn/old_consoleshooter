/*
 * Advanced Methods in Programming 
 * Final Project: Pizza-on-line
 * 
 *   
 *   @author 309170249 317335214
 */
package mutual;

import java.rmi.Remote;
import java.rmi.RemoteException;

import mutual.RMIContracts.Stages;

/**
 * Exposing the callbacks or clients
 */
public interface absClient extends Remote {
	/**
	 * The standard way to pass information from server to client
	 * 
	 * @param stage
	 *            Stage of order or other info.
	 * @throws RemoteException
	 */
	public void notifyStage(Stages stage) throws RemoteException;

	/**
	 * The standard way to pass order details to client
	 * 
	 * @param order
	 *            The relevant order
	 * @throws RemoteException
	 */
	public void orderDetails(Order order) throws RemoteException;
}
