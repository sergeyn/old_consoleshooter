/*
 * Advanced Methods in Programming 
 * Final Project: Pizza-on-line
 * 
 *   
 *   @author 309170249 317335214
 */
package mutual;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Exposing the services of pizzaOnline
 */
public interface absServer extends Remote {
	/**
	 * Perform new orders and updates to old order
	 * 
	 * @param order
	 *            Partially constructed (by client) order instance
	 * @throws RemoteException
	 */
	public void order(Order order) throws RemoteException;

	/**
	 * Getter for old orders
	 * 
	 * @param id
	 *            The id of stored order
	 * 
	 *            Sends client stored order if id is valid else - null # invalid
	 *            ids are - non existing, being processed now, belonging to
	 *            other user ## due to platform limitations we store orders only
	 *            for online users
	 * @see client.pizzaClient.hashString()
	 * @throws RemoteException
	 */
	public void getOrder(String clientName, String id) throws RemoteException;
}
