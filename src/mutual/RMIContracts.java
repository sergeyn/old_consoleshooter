/**
 * Advanced Methods in Programming 
 * Final Project: Pizza-on-line
 * 
 *   
 *   @author 309170249 317335214
 */
package mutual;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;



/**
 * Providing de-facto protocol contracts for server-client relationships
 */
public abstract class RMIContracts {

	/**
	 * Defining well known stages-messages - state of order and/or query to
	 * server
	 */
	public enum Stages {
		ACCEPTED, WAITING, INOVEN, INDELIVERY, DONE, CANTUPDATE
	};

	// This string is used for tests mainly - when we try to get info about
	// order with that id - the server selfdestructs
	public static final String GrimReaper = "Bugs Bunny";
	// This is the name of the server
	public static final String serverName = "pizzaOnline";
	// This is the address of the host
	public static final String serverAddress = "127.0.0.1";
	// This is the port we are going to use
	public static final int serverPort = 3233;

	/**
	 * Register any server on the registry
	 * 
	 * @param registry
	 *            Registry for servers binding
	 * @param name
	 *            Name to be looked upon
	 * @param server
	 *            Any remote server (client is also a server)
	 */
	public static void registerServer(Registry registry, String name,
			Remote server) {
		try {
			registry.rebind(name, server);
		} catch (RemoteException e) {
			System.out.println("Rebinding failed for: " + name);
		}
	}

	/**
	 * Getter for the server by name from registry
	 * 
	 * @param name
	 *            The name of the server we are seeking
	 * @return Reference to remote server (null on failure)
	 */
	public static Remote getServer(String name) {
		Registry registry;
		try {
			// get the “registry”
			registry = LocateRegistry.getRegistry(serverAddress, serverPort);
			// look up the remote object
			return (registry.lookup(name));
		} catch (RemoteException e) {
			System.out.println("Could not get server");
		} catch (NotBoundException e) {
			System.out.println("Could not get server");
		}
		return null;
	}
}