def get_path(key)
	
	home_hash = { 
		'open' => '/home/snepomny/structures/data/open/',
		'TMscore' => '/home/snepomny/structures/progs/TMscore_32',
		'bl2seq' => '/home/snepomny/structures/progs/blast-2.2.22/bin/bl2seq',
		'seq_files' => '/home/snepomny/structures/data/seq',
		'rxyz_files' => '/home/snepomny/structures/data/rxyz/',
		'bio' => 'bio',
		'formatdb' => '/home/snepomny/structures/progs/blast-2.2.22/bin/formatdb',
		'blastall' => '/home/snepomny/structures/progs/blast-2.2.22/bin/blastall'
	}
	

	intel_hash = {  
		'open' => '/nfs/iil/proj/mpgarch/gsr_data_bcs02/BCS/stuff/open/',
		'TMscore' => '/nfs/iil/proj/mpgarch/gsr_data_bcs02/BCS/stuff/TMscore',
		'seq_files' => '/nfs/iil/proj/mpgarch/gsr_data_bcs02/BCS/stuff/seq',
		'bl2seq' => '/nfs/iil/proj/mpgarch/gsr_data_bcs02/BCS/stuff/port/bl2seq',
		'rxyz_files' => '/nfs/iil/proj/mpgarch/gsr_data_bcs02/BCS/stuff/rxyz/',
		'bio' => 'tempfile'
	}
	
	rachel_hash = {  
		'open' => '',
		'TMscore' => '/home/trachel/TM_score/TMscore_32',
		'seq_files' => '/media/disk/rsync_data/seq/',
		'bl2seq' => '/media/disk/blast-2.2.17/bin/bl2seq',
		'rxyz_files' => '/media/disk/rsync_data/rxyz/',
		'bio' => '/home/rsyncuser/.gem/ruby/1.9.0/gems/bio-1.4.0/lib/bio.rb',
		'formatdb' => '/media/disk/blast-2.2.17/bin/formatdb',
		'blastall' => '/media/disk/blast-2.2.17/bin/blastall'
	}
	
	rachel2_hash = {
                'open' => '',
                'TMscore' => '/home/trachel/mt/TM_score/TMscore_32',
                'seq_files' => '/home/snepomny/rsync/pdb/',
                'bl2seq' => '/usr/bin/bl2seq',
                'rxyz_files' => '/home/snepomny/rsync/rxyz/',
                'bio' => '/home/rsyncuser/.gem/ruby/1.9.0/gems/bio-1.4.0/lib/bio.rb',
                'formatdb' => '/usr/bin/formatdb',
                'blastall' => '/usr/bin/blastall'
        }

	return intel_hash[key] if $in_intel
	return rachel_hash[key] if $on_rachel_lx
	return rachel2_hash[key] if $on_rachel2
	return home_hash[key]
end
