# fname = (f = File.new ARGV[0])
# while (line=f.gets)
# 	puts fname + "#" + line.split(':')[-1].strip if line.include? 'REMARK'
# 	fname = line.chomp.strip
# end
#=========================================================
# file = File.new(ARGV[0])
# hsh = {}
# id = nil
# rf = nil
# while line=file.gets do
#   tmp = line.split(':')[1].strip 
#   id = tmp if line.include? 'ID'
#   rf = tmp if line.include? 'R-F'
#   if id and rf
#     hsh[id]=rf
#     id = nil
#     rf = nil
#   end
# end
# 
# hsh.each_pair do |k,v|
#   puts "#{k},#{v}"
# end
#=========================================================
def add_rfactors_from_file(fname)
	f = File.new fname
	while line = f.gets do
		arr = line.split(',')
		set_rfactor_for_protein(arr[0],arr[1])
	end
end