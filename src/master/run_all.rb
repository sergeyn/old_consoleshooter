#!/usr/intel/pkgs/ruby/1.8.2/bin/ruby

$in_intel = (`id`.include? 'mpgall')
$on_rachel_lx = (`whoami`.include? 'rsyncuser')
$on_rachel2 = (`uname -a`.include? 'rachel-2')

kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require 'rubygems'
require kernel_path +'env'
require kernel_path +'pdb_my_lib.rb'
require kernel_path +'log_lib'
require kernel_path +'db_lib'
require kernel_path +'blast_lib'
require kernel_path +'rmsd_lib'
require kernel_path +'sync_lib'
require kernel_path +'parse_rfree'





def connect_to_db
    flag = false
    while ! flag
	    begin
	    connect_to_proteins_db if ! $connection
	    flag = true if  $connection
	    rescue => err
	    flag = false
	    puts err
	    puts "retrying after sleep"
	    sleep(rand(7000))
	    end
    end	
end
#---------------------------------------------------------------
def do_something
   #work_on_flat_pairs ARGV[0]
   #upload_tmscore_data_from_file  ARGV[0]
   #reduce_blast_pairs_to_leaders
   #add_rfactors_from_file(ARGV[0])
   do_full_sync_cycle
end

set_e_to_file "/tmp/sync_errors.log"	
set_l_to_file('/dev/null') if ! ARGV.join('#').include?('-verbose')
connect_to_db #unless $on_rachel2
do_something 	
flush_logs

# flag = true
# while ! flag
# 	begin
# 	connect_to_proteins_db if ! $connection
# 	flag = true if  $connection
# 	rescue => err
# 	flag = false
# 	puts err
#         puts "retrying after sleep"
# 	sleep(rand(7000))
# 	end
# end	

#create_tables
#create_table_closeones
#$connection.query "delete from closeones"
#$connection.query "insert into closeones select * from blast_pairs where rmsd<1 and ident>99"
#build_clusters!


#tmscore_RachelCode ARGV[0].sub('_',''), ARGV[1].sub('_','')
#rmsd_from_db 1, " where l_name = '#{ARGV[0]}' and r_name='#{ARGV[1]}'"

#set_l_to_file ARGV[0] + ".res"
#blast_pairs_from_file ARGV[0],123456

#rmsd_from_file ARGV[0],123456
#create_rxyz_files(get_path('open') +'*.ent')
#load_rxyz '/home/snepomny/structures/data/open/4aahD.rxyz'
#rmsd_from_db 1, " where l_name = '1b0g_A' and r_name='1hsb_A'"

###sanity checks and db creation
#todo get_path stuff
#parsing and storing all data of blast pairs
#--verbose will activate logging

#f = File.new('len_fix')
#5.times { f.gets }
#counter = 0
#while(line = f.gets) do
#	arr = line.split(',')
#	l = arr[0]
#	r = arr[1]
#	if l < r
#		l = arr[1]
#		r = arr[0]
#	end
#	update_len_in_pairs(l,r,arr[2])
#	counter +=1
#	puts "#{counter}" if counter%1024==1
#end
#return
#parse_multi_blast_fix_l('/media/disk/rsync_data/sync_blasto')
#parse_multi_blast '/media/disk/rsync_data/sync_blasto'


#do_full_sync_cycle
#work_on_flat_pairs ARGV[0]
#rebuild_db_after_blast_update
#reduce_blast_pairs_to_leaders
#flush_logs

#rmsd_from_db 1,where l_name = '1a1q_A' and r_name='1jxp_A'"
#'/nfs/iil/proj/mpgarch/gsr_data_bcs02/BCS/stuff/blast_hits.log'
#sleep(rand(3000))
#rmsd_from_file ARGV[0],123456
#rmsd_from_file '/nfs/iil/proj/mpgarch/gsr_data_bcs02/BCS/stuff/blast_hits.log',123456
#dump_blast_hits_to_file '/tmp/allblast'

