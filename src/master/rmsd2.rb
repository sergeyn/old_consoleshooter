seq_files = '/home/snepomny/structures/data/new_seq_files' #'/nfs/iil/proj/mpgarch/gsr_data_bcs02/BCS/stuff/port/new_seq_files'
str_files = '/home/snepomny/structures/data/new_str_files' 
bl2seq = '/home/snepomny/structures/progs/blast-2.2.22/bin/bl2seq'
blast_pdbs = '/home/snepomny/structures/pdb_utils/bin/blast_pdbs'
tmscore = '/home/snepomny/structures/progs/TMscore_32'
prot1 = ARGV[0]
prot2 = ARGV[1]

cmd = " #{bl2seq} -i #{seq_files}/#{prot1}.seq -j #{seq_files}/#{prot2}.seq -p blastp -F F > /tmp/blast"
#puts cmd
%x[#{cmd}]

cmd = "#{blast_pdbs} #{str_files}/#{prot1}.brk  #{str_files}/#{prot2}.brk -B blast > /tmp/_output_"
#puts cmd
%x[#{cmd}]

sline1 = `grep -n MOLECULE /tmp/_output_ | head -n 1 | awk -F: '{print $1}'`
sline2 = `grep -n MOLECULE /tmp/_output_ | tail -n 1 | awk -F: '{print $1}'`
eline1 = `grep -n END /tmp/_output_ | head -n 1 | awk -F: '{print $1}'`
eline2 = `grep -n END /tmp/_output_ | tail -n 1 | awk -F: '{print $1}'`
sline1.chomp!
eline1.chomp!
sline2.chomp!
eline2.chomp!
# 
len1 = eline1.to_i - sline1.to_i
len2 = eline2.to_i - sline2.to_i

%x[head -n #{eline1} /tmp/_output_ | tail -n #{len1} > /tmp/#{prot1}]
%x[head -n #{eline2} /tmp/_output_ | tail -n #{len2} > /tmp/#{prot2}]
puts "#{tmscore} /tmp/#{prot1} /tmp/#{prot2} "
print %x[#{tmscore} /tmp/#{prot1} /tmp/#{prot2} | grep -e ^RMSD -e ^TM-score -e ^GDT-TS -e ^GDT-HA -e ^Max -e ^Structure]

#head -n 2 /tmp/_output_

#chmod 777 /tmp/$prot1
#chmod 777 /tmp/$prot2
#chmod 777 /tmp/_output_
#chmod 777 /tmp/blast
#rm /tmp/$prot1
#rm /tmp/$prot2
#rm /tmp/_output_
#rm /tmp/blast


