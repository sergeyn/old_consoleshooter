#init the  globals 
$WARN_IO = $stdout if !$WARN_IO
$DEBUG_IO = $stdout if !$DEBUG_IO
$ERR_IO = $stderr if !$ERR_IO
$LOG_IO = $stdout if !$LOG_IO

def set_l_to_file(fname)
	$LOG_IO = File.new(fname,'w')
end

def set_w_to_file(fname)
	$WARN_IO = File.new(fname,'w')
	$WARN_IO.sync = true
end

def set_e_to_file(fname)
	$ERR_IO = File.new(fname,'w')
	$ERR_IO.sync = true
end

def warning_log string
	$WARN_IO.puts string
end

def debug_log string
	$DEBUG_IO.puts string
end

def err_log string
	$ERR_IO.puts string
end

def log_me string
	$LOG_IO.puts string
end

def flush_logs
	$WARN_IO.flush
	$DEBUG_IO.flush
	$ERR_IO.flush
	$LOG_IO.flush
end



