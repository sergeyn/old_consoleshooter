/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x141a37e9 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/snepomny/Downloads/new.v";
static int ng1[] = {0, 0};
static int ng2[] = {1, 0};
static int ng3[] = {5, 0};
static int ng4[] = {2, 0};
static int ng5[] = {13, 0};
static int ng6[] = {6, 0};



static void Always_209_0(char *t0)
{
    char t6[8];
    char t11[8];
    char t35[8];
    char t42[8];
    char t43[8];
    char t44[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    char *t26;
    char *t27;
    unsigned int t28;
    unsigned int t29;
    unsigned int t30;
    unsigned int t31;
    unsigned int t32;
    char *t33;
    char *t34;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    char *t49;
    char *t50;
    unsigned int t51;
    int t52;
    char *t53;
    unsigned int t54;
    int t55;
    int t56;
    char *t57;
    unsigned int t58;
    int t59;
    int t60;
    unsigned int t61;
    int t62;
    unsigned int t63;
    unsigned int t64;
    int t65;
    int t66;

LAB0:    t1 = (t0 + 1924U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(209, ng0);
    t2 = (t0 + 2104);
    *((int *)t2) = 1;
    t3 = (t0 + 1948);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(210, ng0);

LAB5:    xsi_set_current_line(211, ng0);
    t4 = (t0 + 1000U);
    t5 = *((char **)t4);
    t4 = (t0 + 976U);
    t7 = (t4 + 44U);
    t8 = *((char **)t7);
    t9 = ((char*)((ng1)));
    xsi_vlog_generic_get_index_select_value(t6, 32, t5, t8, 2, t9, 32, 1);
    t10 = ((char*)((ng1)));
    memset(t11, 0, 8);
    t12 = (t6 + 4);
    t13 = (t10 + 4);
    t14 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t10);
    t16 = (t14 ^ t15);
    t17 = *((unsigned int *)t12);
    t18 = *((unsigned int *)t13);
    t19 = (t17 ^ t18);
    t20 = (t16 | t19);
    t21 = *((unsigned int *)t12);
    t22 = *((unsigned int *)t13);
    t23 = (t21 | t22);
    t24 = (~(t23));
    t25 = (t20 & t24);
    if (t25 != 0)
        goto LAB9;

LAB6:    if (t23 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t11) = 1;

LAB9:    t27 = (t11 + 4);
    t28 = *((unsigned int *)t27);
    t29 = (~(t28));
    t30 = *((unsigned int *)t11);
    t31 = (t30 & t29);
    t32 = (t31 != 0);
    if (t32 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(214, ng0);

LAB14:    xsi_set_current_line(215, ng0);
    t2 = (t0 + 1000U);
    t3 = *((char **)t2);
    t2 = (t0 + 976U);
    t4 = (t2 + 44U);
    t5 = *((char **)t4);
    t7 = ((char*)((ng2)));
    xsi_vlog_generic_get_index_select_value(t6, 32, t3, t5, 2, t7, 32, 1);
    t8 = ((char*)((ng1)));
    memset(t11, 0, 8);
    t9 = (t6 + 4);
    t10 = (t8 + 4);
    t14 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t8);
    t16 = (t14 ^ t15);
    t17 = *((unsigned int *)t9);
    t18 = *((unsigned int *)t10);
    t19 = (t17 ^ t18);
    t20 = (t16 | t19);
    t21 = *((unsigned int *)t9);
    t22 = *((unsigned int *)t10);
    t23 = (t21 | t22);
    t24 = (~(t23));
    t25 = (t20 & t24);
    if (t25 != 0)
        goto LAB18;

LAB15:    if (t23 != 0)
        goto LAB17;

LAB16:    *((unsigned int *)t11) = 1;

LAB18:    t13 = (t11 + 4);
    t28 = *((unsigned int *)t13);
    t29 = (~(t28));
    t30 = *((unsigned int *)t11);
    t31 = (t30 & t29);
    t32 = (t31 != 0);
    if (t32 > 0)
        goto LAB19;

LAB20:    xsi_set_current_line(220, ng0);

LAB29:    xsi_set_current_line(223, ng0);
    t2 = (t0 + 1000U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t14 = *((unsigned int *)t3);
    t15 = (t14 >> 2);
    *((unsigned int *)t6) = t15;
    t16 = *((unsigned int *)t4);
    t17 = (t16 >> 2);
    *((unsigned int *)t2) = t17;
    t18 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t18 & 15U);
    t19 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t19 & 15U);
    t5 = (t0 + 1412);
    t7 = (t0 + 1412);
    t8 = (t7 + 44U);
    t9 = *((char **)t8);
    t10 = (t0 + 1412);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    t26 = (t0 + 1000U);
    t27 = *((char **)t26);
    memset(t42, 0, 8);
    t26 = (t42 + 4);
    t33 = (t27 + 4);
    t20 = *((unsigned int *)t27);
    t21 = (t20 >> 8);
    *((unsigned int *)t42) = t21;
    t22 = *((unsigned int *)t33);
    t23 = (t22 >> 8);
    *((unsigned int *)t26) = t23;
    t24 = *((unsigned int *)t42);
    *((unsigned int *)t42) = (t24 & 15U);
    t25 = *((unsigned int *)t26);
    *((unsigned int *)t26) = (t25 & 15U);
    xsi_vlog_generic_convert_array_indices(t11, t35, t9, t13, 2, 1, t42, 4, 2);
    t34 = (t11 + 4);
    t28 = *((unsigned int *)t34);
    t52 = (!(t28));
    t45 = (t35 + 4);
    t29 = *((unsigned int *)t45);
    t55 = (!(t29));
    t56 = (t52 && t55);
    if (t56 == 1)
        goto LAB30;

LAB31:    xsi_set_current_line(225, ng0);
    t2 = (t0 + 1000U);
    t3 = *((char **)t2);
    t2 = (t0 + 1320);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 14, 0LL);

LAB21:
LAB12:    goto LAB2;

LAB8:    t26 = (t11 + 4);
    *((unsigned int *)t11) = 1;
    *((unsigned int *)t26) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(211, ng0);

LAB13:    xsi_set_current_line(212, ng0);
    t33 = ((char*)((ng1)));
    t34 = (t0 + 1320);
    xsi_vlogvar_wait_assign_value(t34, t33, 0, 0, 14, 0LL);
    goto LAB12;

LAB17:    t12 = (t11 + 4);
    *((unsigned int *)t11) = 1;
    *((unsigned int *)t12) = 1;
    goto LAB18;

LAB19:    xsi_set_current_line(215, ng0);

LAB22:    xsi_set_current_line(216, ng0);
    t26 = (t0 + 1000U);
    t27 = *((char **)t26);
    memset(t35, 0, 8);
    t26 = (t35 + 4);
    t33 = (t27 + 4);
    t36 = *((unsigned int *)t27);
    t37 = (t36 >> 0);
    *((unsigned int *)t35) = t37;
    t38 = *((unsigned int *)t33);
    t39 = (t38 >> 0);
    *((unsigned int *)t26) = t39;
    t40 = *((unsigned int *)t35);
    *((unsigned int *)t35) = (t40 & 3U);
    t41 = *((unsigned int *)t26);
    *((unsigned int *)t26) = (t41 & 3U);
    t34 = (t0 + 1320);
    t45 = (t0 + 1320);
    t46 = (t45 + 44U);
    t47 = *((char **)t46);
    t48 = ((char*)((ng2)));
    t49 = ((char*)((ng1)));
    xsi_vlog_convert_partindices(t42, t43, t44, ((int*)(t47)), 2, t48, 32, 1, t49, 32, 1);
    t50 = (t42 + 4);
    t51 = *((unsigned int *)t50);
    t52 = (!(t51));
    t53 = (t43 + 4);
    t54 = *((unsigned int *)t53);
    t55 = (!(t54));
    t56 = (t52 && t55);
    t57 = (t44 + 4);
    t58 = *((unsigned int *)t57);
    t59 = (!(t58));
    t60 = (t56 && t59);
    if (t60 == 1)
        goto LAB23;

LAB24:    xsi_set_current_line(217, ng0);
    t2 = (t0 + 1412);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t0 + 1412);
    t7 = (t5 + 44U);
    t8 = *((char **)t7);
    t9 = (t0 + 1412);
    t10 = (t9 + 40U);
    t12 = *((char **)t10);
    t13 = (t0 + 1000U);
    t26 = *((char **)t13);
    memset(t11, 0, 8);
    t13 = (t11 + 4);
    t27 = (t26 + 4);
    t14 = *((unsigned int *)t26);
    t15 = (t14 >> 8);
    *((unsigned int *)t11) = t15;
    t16 = *((unsigned int *)t27);
    t17 = (t16 >> 8);
    *((unsigned int *)t13) = t17;
    t18 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t18 & 15U);
    t19 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t19 & 15U);
    xsi_vlog_generic_get_array_select_value(t6, 4, t4, t8, t12, 2, 1, t11, 4, 2);
    t33 = (t0 + 1320);
    t34 = (t0 + 1320);
    t45 = (t34 + 44U);
    t46 = *((char **)t45);
    t47 = ((char*)((ng3)));
    t48 = ((char*)((ng4)));
    xsi_vlog_convert_partindices(t35, t42, t43, ((int*)(t46)), 2, t47, 32, 1, t48, 32, 1);
    t49 = (t35 + 4);
    t20 = *((unsigned int *)t49);
    t52 = (!(t20));
    t50 = (t42 + 4);
    t21 = *((unsigned int *)t50);
    t55 = (!(t21));
    t56 = (t52 && t55);
    t53 = (t43 + 4);
    t22 = *((unsigned int *)t53);
    t59 = (!(t22));
    t60 = (t56 && t59);
    if (t60 == 1)
        goto LAB25;

LAB26:    xsi_set_current_line(218, ng0);
    t2 = (t0 + 1000U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t14 = *((unsigned int *)t3);
    t15 = (t14 >> 6);
    *((unsigned int *)t6) = t15;
    t16 = *((unsigned int *)t4);
    t17 = (t16 >> 6);
    *((unsigned int *)t2) = t17;
    t18 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t18 & 255U);
    t19 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t19 & 255U);
    t5 = (t0 + 1320);
    t7 = (t0 + 1320);
    t8 = (t7 + 44U);
    t9 = *((char **)t8);
    t10 = ((char*)((ng5)));
    t12 = ((char*)((ng6)));
    xsi_vlog_convert_partindices(t11, t35, t42, ((int*)(t9)), 2, t10, 32, 1, t12, 32, 1);
    t13 = (t11 + 4);
    t20 = *((unsigned int *)t13);
    t52 = (!(t20));
    t26 = (t35 + 4);
    t21 = *((unsigned int *)t26);
    t55 = (!(t21));
    t56 = (t52 && t55);
    t27 = (t42 + 4);
    t22 = *((unsigned int *)t27);
    t59 = (!(t22));
    t60 = (t56 && t59);
    if (t60 == 1)
        goto LAB27;

LAB28:    goto LAB21;

LAB23:    t61 = *((unsigned int *)t44);
    t62 = (t61 + 0);
    t63 = *((unsigned int *)t42);
    t64 = *((unsigned int *)t43);
    t65 = (t63 - t64);
    t66 = (t65 + 1);
    xsi_vlogvar_wait_assign_value(t34, t35, t62, *((unsigned int *)t43), t66, 0LL);
    goto LAB24;

LAB25:    t23 = *((unsigned int *)t43);
    t62 = (t23 + 0);
    t24 = *((unsigned int *)t35);
    t25 = *((unsigned int *)t42);
    t65 = (t24 - t25);
    t66 = (t65 + 1);
    xsi_vlogvar_wait_assign_value(t33, t6, t62, *((unsigned int *)t42), t66, 0LL);
    goto LAB26;

LAB27:    t23 = *((unsigned int *)t42);
    t62 = (t23 + 0);
    t24 = *((unsigned int *)t11);
    t25 = *((unsigned int *)t35);
    t65 = (t24 - t25);
    t66 = (t65 + 1);
    xsi_vlogvar_wait_assign_value(t5, t6, t62, *((unsigned int *)t35), t66, 0LL);
    goto LAB28;

LAB30:    t30 = *((unsigned int *)t11);
    t31 = *((unsigned int *)t35);
    t59 = (t30 - t31);
    t60 = (t59 + 1);
    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t35), t60, 0LL);
    goto LAB31;

}


extern void work_m_00000000000269377920_3937639625_init()
{
	static char *pe[] = {(void *)Always_209_0};
	xsi_register_didat("work_m_00000000000269377920_3937639625", "isim/Test_Fullchip_isim_beh.exe.sim/work/m_00000000000269377920_3937639625.didat");
	xsi_register_executes(pe);
}
