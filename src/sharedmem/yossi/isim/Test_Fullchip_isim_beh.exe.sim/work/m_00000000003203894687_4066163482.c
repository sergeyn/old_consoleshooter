/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x141a37e9 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "%d(%b) TIME %d: in \t (%b|%b|%b|%b)";
static const char *ng1 = "%d(%b) TIME %d: ==> out \t (%b|%b|%b|%b)";
static const char *ng2 = "/home/snepomny/Downloads/new.v";
static int ng3[] = {0, 0};
static int ng4[] = {4, 0};
static unsigned int ng5[] = {3U, 0U};
static int ng6[] = {1, 0};

void Monitor_347_3(char *);
void Monitor_354_5(char *);
void Monitor_347_3(char *);
void Monitor_354_5(char *);


static void Monitor_347_3_Func(char *t0)
{
    char t1[8];
    char t13[8];
    char t25[16];
    char t27[8];
    char t40[8];
    char t60[8];
    char t72[8];
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    char *t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    char *t50;
    char *t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    char *t61;
    char *t62;
    char *t63;
    char *t64;
    char *t65;
    unsigned int t66;
    unsigned int t67;
    unsigned int t68;
    unsigned int t69;
    unsigned int t70;
    unsigned int t71;
    char *t73;
    char *t74;
    char *t75;
    char *t76;
    char *t77;
    unsigned int t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;

LAB0:    t2 = (t0 + 1916);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    memset(t1, 0, 8);
    t5 = (t1 + 4);
    t6 = (t4 + 4);
    t7 = *((unsigned int *)t4);
    t8 = (t7 >> 0);
    *((unsigned int *)t1) = t8;
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 0);
    *((unsigned int *)t5) = t10;
    t11 = *((unsigned int *)t1);
    *((unsigned int *)t1) = (t11 & 15U);
    t12 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t12 & 15U);
    t14 = (t0 + 1916);
    t15 = (t14 + 36U);
    t16 = *((char **)t15);
    memset(t13, 0, 8);
    t17 = (t13 + 4);
    t18 = (t16 + 4);
    t19 = *((unsigned int *)t16);
    t20 = (t19 >> 0);
    *((unsigned int *)t13) = t20;
    t21 = *((unsigned int *)t18);
    t22 = (t21 >> 0);
    *((unsigned int *)t17) = t22;
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 15U);
    t24 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t24 & 15U);
    t26 = xsi_vlog_time(t25, 1.0000000000000000, 1.0000000000000000);
    t28 = (t0 + 1824);
    t29 = (t28 + 36U);
    t30 = *((char **)t29);
    memset(t27, 0, 8);
    t31 = (t27 + 4);
    t32 = (t30 + 8);
    t33 = (t30 + 12);
    t34 = *((unsigned int *)t32);
    t35 = (t34 >> 10);
    *((unsigned int *)t27) = t35;
    t36 = *((unsigned int *)t33);
    t37 = (t36 >> 10);
    *((unsigned int *)t31) = t37;
    t38 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t38 & 16383U);
    t39 = *((unsigned int *)t31);
    *((unsigned int *)t31) = (t39 & 16383U);
    t41 = (t0 + 1824);
    t42 = (t41 + 36U);
    t43 = *((char **)t42);
    memset(t40, 0, 8);
    t44 = (t40 + 4);
    t45 = (t43 + 4);
    t46 = *((unsigned int *)t43);
    t47 = (t46 >> 28);
    *((unsigned int *)t40) = t47;
    t48 = *((unsigned int *)t45);
    t49 = (t48 >> 28);
    *((unsigned int *)t44) = t49;
    t50 = (t43 + 8);
    t51 = (t43 + 12);
    t52 = *((unsigned int *)t50);
    t53 = (t52 << 4);
    t54 = *((unsigned int *)t40);
    *((unsigned int *)t40) = (t54 | t53);
    t55 = *((unsigned int *)t51);
    t56 = (t55 << 4);
    t57 = *((unsigned int *)t44);
    *((unsigned int *)t44) = (t57 | t56);
    t58 = *((unsigned int *)t40);
    *((unsigned int *)t40) = (t58 & 16383U);
    t59 = *((unsigned int *)t44);
    *((unsigned int *)t44) = (t59 & 16383U);
    t61 = (t0 + 1824);
    t62 = (t61 + 36U);
    t63 = *((char **)t62);
    memset(t60, 0, 8);
    t64 = (t60 + 4);
    t65 = (t63 + 4);
    t66 = *((unsigned int *)t63);
    t67 = (t66 >> 14);
    *((unsigned int *)t60) = t67;
    t68 = *((unsigned int *)t65);
    t69 = (t68 >> 14);
    *((unsigned int *)t64) = t69;
    t70 = *((unsigned int *)t60);
    *((unsigned int *)t60) = (t70 & 16383U);
    t71 = *((unsigned int *)t64);
    *((unsigned int *)t64) = (t71 & 16383U);
    t73 = (t0 + 1824);
    t74 = (t73 + 36U);
    t75 = *((char **)t74);
    memset(t72, 0, 8);
    t76 = (t72 + 4);
    t77 = (t75 + 4);
    t78 = *((unsigned int *)t75);
    t79 = (t78 >> 0);
    *((unsigned int *)t72) = t79;
    t80 = *((unsigned int *)t77);
    t81 = (t80 >> 0);
    *((unsigned int *)t76) = t81;
    t82 = *((unsigned int *)t72);
    *((unsigned int *)t72) = (t82 & 16383U);
    t83 = *((unsigned int *)t76);
    *((unsigned int *)t76) = (t83 & 16383U);
    xsi_vlogfile_write(1, 0, 3, ng0, 8, t0, (char)118, t1, 4, (char)118, t13, 4, (char)118, t25, 64, (char)118, t27, 14, (char)118, t40, 14, (char)118, t60, 14, (char)118, t72, 14);

LAB1:    return;
}

static void Monitor_354_5_Func(char *t0)
{
    char t1[8];
    char t13[8];
    char t25[16];
    char t27[8];
    char t38[8];
    char t56[8];
    char t66[8];
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    char *t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    unsigned int t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    char *t39;
    char *t40;
    char *t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    char *t46;
    char *t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    unsigned int t55;
    char *t57;
    char *t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    char *t67;
    char *t68;
    char *t69;
    unsigned int t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;

LAB0:    t2 = (t0 + 1916);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    memset(t1, 0, 8);
    t5 = (t1 + 4);
    t6 = (t4 + 4);
    t7 = *((unsigned int *)t4);
    t8 = (t7 >> 0);
    *((unsigned int *)t1) = t8;
    t9 = *((unsigned int *)t6);
    t10 = (t9 >> 0);
    *((unsigned int *)t5) = t10;
    t11 = *((unsigned int *)t1);
    *((unsigned int *)t1) = (t11 & 15U);
    t12 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t12 & 15U);
    t14 = (t0 + 1916);
    t15 = (t14 + 36U);
    t16 = *((char **)t15);
    memset(t13, 0, 8);
    t17 = (t13 + 4);
    t18 = (t16 + 4);
    t19 = *((unsigned int *)t16);
    t20 = (t19 >> 0);
    *((unsigned int *)t13) = t20;
    t21 = *((unsigned int *)t18);
    t22 = (t21 >> 0);
    *((unsigned int *)t17) = t22;
    t23 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t23 & 15U);
    t24 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t24 & 15U);
    t26 = xsi_vlog_time(t25, 1.0000000000000000, 1.0000000000000000);
    t28 = (t0 + 1320U);
    t29 = *((char **)t28);
    memset(t27, 0, 8);
    t28 = (t27 + 4);
    t30 = (t29 + 8);
    t31 = (t29 + 12);
    t32 = *((unsigned int *)t30);
    t33 = (t32 >> 10);
    *((unsigned int *)t27) = t33;
    t34 = *((unsigned int *)t31);
    t35 = (t34 >> 10);
    *((unsigned int *)t28) = t35;
    t36 = *((unsigned int *)t27);
    *((unsigned int *)t27) = (t36 & 16383U);
    t37 = *((unsigned int *)t28);
    *((unsigned int *)t28) = (t37 & 16383U);
    t39 = (t0 + 1320U);
    t40 = *((char **)t39);
    memset(t38, 0, 8);
    t39 = (t38 + 4);
    t41 = (t40 + 4);
    t42 = *((unsigned int *)t40);
    t43 = (t42 >> 28);
    *((unsigned int *)t38) = t43;
    t44 = *((unsigned int *)t41);
    t45 = (t44 >> 28);
    *((unsigned int *)t39) = t45;
    t46 = (t40 + 8);
    t47 = (t40 + 12);
    t48 = *((unsigned int *)t46);
    t49 = (t48 << 4);
    t50 = *((unsigned int *)t38);
    *((unsigned int *)t38) = (t50 | t49);
    t51 = *((unsigned int *)t47);
    t52 = (t51 << 4);
    t53 = *((unsigned int *)t39);
    *((unsigned int *)t39) = (t53 | t52);
    t54 = *((unsigned int *)t38);
    *((unsigned int *)t38) = (t54 & 16383U);
    t55 = *((unsigned int *)t39);
    *((unsigned int *)t39) = (t55 & 16383U);
    t57 = (t0 + 1320U);
    t58 = *((char **)t57);
    memset(t56, 0, 8);
    t57 = (t56 + 4);
    t59 = (t58 + 4);
    t60 = *((unsigned int *)t58);
    t61 = (t60 >> 14);
    *((unsigned int *)t56) = t61;
    t62 = *((unsigned int *)t59);
    t63 = (t62 >> 14);
    *((unsigned int *)t57) = t63;
    t64 = *((unsigned int *)t56);
    *((unsigned int *)t56) = (t64 & 16383U);
    t65 = *((unsigned int *)t57);
    *((unsigned int *)t57) = (t65 & 16383U);
    t67 = (t0 + 1320U);
    t68 = *((char **)t67);
    memset(t66, 0, 8);
    t67 = (t66 + 4);
    t69 = (t68 + 4);
    t70 = *((unsigned int *)t68);
    t71 = (t70 >> 0);
    *((unsigned int *)t66) = t71;
    t72 = *((unsigned int *)t69);
    t73 = (t72 >> 0);
    *((unsigned int *)t67) = t73;
    t74 = *((unsigned int *)t66);
    *((unsigned int *)t66) = (t74 & 16383U);
    t75 = *((unsigned int *)t67);
    *((unsigned int *)t67) = (t75 & 16383U);
    xsi_vlogfile_write(1, 0, 3, ng1, 8, t0, (char)118, t1, 4, (char)118, t13, 4, (char)118, t25, 64, (char)118, t27, 14, (char)118, t38, 14, (char)118, t56, 14, (char)118, t66, 14);

LAB1:    return;
}

static void Initial_311_0(char *t0)
{
    char t6[8];
    char t15[8];
    char t18[16];
    char t30[8];
    char t43[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    char *t14;
    char *t16;
    char *t17;
    unsigned int t19;
    char *t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    char *t42;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    unsigned int t54;
    char *t55;

LAB0:    t1 = (t0 + 2612U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(311, ng2);

LAB4:    xsi_set_current_line(313, ng2);
    xsi_set_current_line(313, ng2);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 1916);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 32);

LAB5:    t2 = (t0 + 1916);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng4)));
    memset(t6, 0, 8);
    xsi_vlog_signed_less(t6, 32, t4, 32, t5, 32);
    t7 = (t6 + 4);
    t8 = *((unsigned int *)t7);
    t9 = (~(t8));
    t10 = *((unsigned int *)t6);
    t11 = (t10 & t9);
    t12 = (t11 != 0);
    if (t12 > 0)
        goto LAB6;

LAB7:    xsi_set_current_line(340, ng2);
    t2 = (t0 + 2512);
    xsi_process_wait(t2, 6LL);
    *((char **)t1) = &&LAB11;

LAB1:    return;
LAB6:    xsi_set_current_line(313, ng2);

LAB8:    xsi_set_current_line(315, ng2);
    t13 = (t0 + 3472);
    *((int *)t13) = 1;
    t14 = (t0 + 2636);
    *((char **)t14) = t13;
    *((char **)t1) = &&LAB9;
    goto LAB1;

LAB9:    xsi_set_current_line(315, ng2);

LAB10:    xsi_set_current_line(318, ng2);
    *((int *)t15) = xsi_vlog_rtl_dist_uniform(0, 0, -2147483648, 2147483647);
    t16 = (t15 + 4);
    *((int *)t16) = 0;
    t17 = (t0 + 2008);
    xsi_vlogvar_assign_value(t17, t15, 0, 0, 32);
    xsi_set_current_line(319, ng2);
    *((int *)t6) = xsi_vlog_rtl_dist_uniform(0, 0, -2147483648, 2147483647);
    t2 = (t6 + 4);
    *((int *)t2) = 0;
    t3 = (t0 + 2100);
    xsi_vlogvar_assign_value(t3, t6, 0, 0, 32);
    xsi_set_current_line(320, ng2);
    t2 = ((char*)((ng5)));
    t3 = (t0 + 2100);
    t4 = (t3 + 36U);
    t5 = *((char **)t4);
    memset(t6, 0, 8);
    t7 = (t6 + 4);
    t13 = (t5 + 4);
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 12);
    *((unsigned int *)t6) = t9;
    t10 = *((unsigned int *)t13);
    t11 = (t10 >> 12);
    *((unsigned int *)t7) = t11;
    t12 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t12 & 4095U);
    t19 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t19 & 4095U);
    t14 = ((char*)((ng5)));
    t16 = (t0 + 2100);
    t17 = (t16 + 36U);
    t20 = *((char **)t17);
    memset(t15, 0, 8);
    t21 = (t15 + 4);
    t22 = (t20 + 4);
    t23 = *((unsigned int *)t20);
    t24 = (t23 >> 0);
    *((unsigned int *)t15) = t24;
    t25 = *((unsigned int *)t22);
    t26 = (t25 >> 0);
    *((unsigned int *)t21) = t26;
    t27 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t27 & 4095U);
    t28 = *((unsigned int *)t21);
    *((unsigned int *)t21) = (t28 & 4095U);
    t29 = ((char*)((ng5)));
    t31 = (t0 + 2008);
    t32 = (t31 + 36U);
    t33 = *((char **)t32);
    memset(t30, 0, 8);
    t34 = (t30 + 4);
    t35 = (t33 + 4);
    t36 = *((unsigned int *)t33);
    t37 = (t36 >> 12);
    *((unsigned int *)t30) = t37;
    t38 = *((unsigned int *)t35);
    t39 = (t38 >> 12);
    *((unsigned int *)t34) = t39;
    t40 = *((unsigned int *)t30);
    *((unsigned int *)t30) = (t40 & 4095U);
    t41 = *((unsigned int *)t34);
    *((unsigned int *)t34) = (t41 & 4095U);
    t42 = ((char*)((ng5)));
    t44 = (t0 + 2008);
    t45 = (t44 + 36U);
    t46 = *((char **)t45);
    memset(t43, 0, 8);
    t47 = (t43 + 4);
    t48 = (t46 + 4);
    t49 = *((unsigned int *)t46);
    t50 = (t49 >> 0);
    *((unsigned int *)t43) = t50;
    t51 = *((unsigned int *)t48);
    t52 = (t51 >> 0);
    *((unsigned int *)t47) = t52;
    t53 = *((unsigned int *)t43);
    *((unsigned int *)t43) = (t53 & 4095U);
    t54 = *((unsigned int *)t47);
    *((unsigned int *)t47) = (t54 & 4095U);
    xsi_vlogtype_concat(t18, 56, 56, 8U, t43, 12, t42, 2, t30, 12, t29, 2, t15, 12, t14, 2, t6, 12, t2, 2);
    t55 = (t0 + 1824);
    xsi_vlogvar_assign_value(t55, t18, 0, 0, 56);
    xsi_set_current_line(313, ng2);
    t2 = (t0 + 1916);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng6)));
    memset(t6, 0, 8);
    xsi_vlog_signed_add(t6, 32, t4, 32, t5, 32);
    t7 = (t0 + 1916);
    xsi_vlogvar_assign_value(t7, t6, 0, 0, 32);
    goto LAB5;

LAB11:    xsi_set_current_line(340, ng2);
    xsi_vlog_finish(1);
    goto LAB1;

}

static void Always_343_1(char *t0)
{
    char t3[8];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    char *t14;

LAB0:    t1 = (t0 + 2748U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(343, ng2);
    t2 = (t0 + 2648);
    xsi_process_wait(t2, 1LL);
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(343, ng2);
    t4 = (t0 + 1732);
    t5 = (t4 + 36U);
    t6 = *((char **)t5);
    memset(t3, 0, 8);
    t7 = (t6 + 4);
    t8 = *((unsigned int *)t7);
    t9 = (~(t8));
    t10 = *((unsigned int *)t6);
    t11 = (t10 & t9);
    t12 = (t11 & 1U);
    if (t12 != 0)
        goto LAB8;

LAB6:    if (*((unsigned int *)t7) == 0)
        goto LAB5;

LAB7:    t13 = (t3 + 4);
    *((unsigned int *)t3) = 1;
    *((unsigned int *)t13) = 1;

LAB8:    t14 = (t0 + 1732);
    xsi_vlogvar_assign_value(t14, t3, 0, 0, 1);
    goto LAB2;

LAB5:    *((unsigned int *)t3) = 1;
    goto LAB8;

}

static void Always_345_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;

LAB0:    t1 = (t0 + 2884U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(345, ng2);
    t2 = (t0 + 3480);
    *((int *)t2) = 1;
    t3 = (t0 + 2908);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(346, ng2);

LAB5:    xsi_set_current_line(347, ng2);
    Monitor_347_3(t0);
    goto LAB2;

}

static void Always_352_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;

LAB0:    t1 = (t0 + 3020U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(352, ng2);
    t2 = (t0 + 3488);
    *((int *)t2) = 1;
    t3 = (t0 + 3044);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(353, ng2);

LAB5:    xsi_set_current_line(354, ng2);
    Monitor_354_5(t0);
    goto LAB2;

}

void Monitor_347_3(char *t0)
{
    char *t1;
    char *t2;

LAB0:    t1 = (t0 + 3056);
    t2 = (t0 + 3496);
    xsi_vlogfile_monitor((void *)Monitor_347_3_Func, t1, t2);

LAB1:    return;
}

void Monitor_354_5(char *t0)
{
    char *t1;
    char *t2;

LAB0:    t1 = (t0 + 3192);
    t2 = (t0 + 3504);
    xsi_vlogfile_monitor((void *)Monitor_354_5_Func, t1, t2);

LAB1:    return;
}


extern void work_m_00000000003203894687_4066163482_init()
{
	static char *pe[] = {(void *)Initial_311_0,(void *)Always_343_1,(void *)Always_345_2,(void *)Always_352_4,(void *)Monitor_347_3,(void *)Monitor_354_5};
	xsi_register_didat("work_m_00000000003203894687_4066163482", "isim/Test_Fullchip_isim_beh.exe.sim/work/m_00000000003203894687_4066163482.didat");
	xsi_register_executes(pe);
}
