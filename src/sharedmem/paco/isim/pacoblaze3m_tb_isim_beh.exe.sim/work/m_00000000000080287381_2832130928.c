/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x141a37e9 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/snepomny/VLSI/paco/pacoblaze-2.2/pacoblaze/blockram.v";
static unsigned int ng1[] = {4294967295U, 4294967295U};



static void Always_46_0(char *t0)
{
    char t18[8];
    char t19[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    unsigned int t28;
    int t29;
    char *t30;
    unsigned int t31;
    int t32;
    int t33;
    unsigned int t34;
    unsigned int t35;
    int t36;
    int t37;

LAB0:    t1 = (t0 + 2132U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(46, ng0);
    t2 = (t0 + 2312);
    *((int *)t2) = 1;
    t3 = (t0 + 2156);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(47, ng0);
    t4 = (t0 + 932U);
    t5 = *((char **)t4);
    t4 = (t5 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB5;

LAB6:    xsi_set_current_line(48, ng0);
    t2 = (t0 + 1024U);
    t3 = *((char **)t2);
    t2 = (t3 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t3);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB8;

LAB9:
LAB10:
LAB7:    goto LAB2;

LAB5:    xsi_set_current_line(47, ng0);
    t11 = ((char*)((ng1)));
    t12 = (t0 + 1528);
    xsi_vlogvar_wait_assign_value(t12, t11, 0, 0, 8, 0LL);
    goto LAB7;

LAB8:    xsi_set_current_line(49, ng0);
    t4 = (t0 + 1116U);
    t5 = *((char **)t4);
    t4 = (t5 + 4);
    t13 = *((unsigned int *)t4);
    t14 = (~(t13));
    t15 = *((unsigned int *)t5);
    t16 = (t15 & t14);
    t17 = (t16 != 0);
    if (t17 > 0)
        goto LAB11;

LAB12:    xsi_set_current_line(50, ng0);
    t2 = (t0 + 1620);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t5 = (t0 + 1620);
    t11 = (t5 + 44U);
    t12 = *((char **)t11);
    t20 = (t0 + 1620);
    t21 = (t20 + 40U);
    t22 = *((char **)t21);
    t23 = (t0 + 1208U);
    t24 = *((char **)t23);
    xsi_vlog_generic_get_array_select_value(t18, 8, t4, t12, t22, 2, 1, t24, 6, 2);
    t23 = (t0 + 1528);
    xsi_vlogvar_wait_assign_value(t23, t18, 0, 0, 8, 0LL);

LAB13:    goto LAB10;

LAB11:    xsi_set_current_line(49, ng0);
    t11 = (t0 + 1300U);
    t12 = *((char **)t11);
    t11 = (t0 + 1620);
    t20 = (t0 + 1620);
    t21 = (t20 + 44U);
    t22 = *((char **)t21);
    t23 = (t0 + 1620);
    t24 = (t23 + 40U);
    t25 = *((char **)t24);
    t26 = (t0 + 1208U);
    t27 = *((char **)t26);
    xsi_vlog_generic_convert_array_indices(t18, t19, t22, t25, 2, 1, t27, 6, 2);
    t26 = (t18 + 4);
    t28 = *((unsigned int *)t26);
    t29 = (!(t28));
    t30 = (t19 + 4);
    t31 = *((unsigned int *)t30);
    t32 = (!(t31));
    t33 = (t29 && t32);
    if (t33 == 1)
        goto LAB14;

LAB15:    goto LAB13;

LAB14:    t34 = *((unsigned int *)t18);
    t35 = *((unsigned int *)t19);
    t36 = (t34 - t35);
    t37 = (t36 + 1);
    xsi_vlogvar_wait_assign_value(t11, t12, 0, *((unsigned int *)t19), t37, 0LL);
    goto LAB15;

}


extern void work_m_00000000000080287381_2832130928_init()
{
	static char *pe[] = {(void *)Always_46_0};
	xsi_register_didat("work_m_00000000000080287381_2832130928", "isim/pacoblaze3m_tb_isim_beh.exe.sim/work/m_00000000000080287381_2832130928.didat");
	xsi_register_executes(pe);
}
