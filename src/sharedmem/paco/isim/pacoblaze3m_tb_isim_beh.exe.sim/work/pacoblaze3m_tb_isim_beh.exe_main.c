/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000000080287381_1436093373_init();
    work_m_00000000000080287381_2832130928_init();
    work_m_00000000003979877312_3494281417_init();
    work_m_00000000000947111324_0378297209_init();
    work_m_00000000003207246685_4289652936_init();
    work_m_00000000003851753293_1411583278_init();
    work_m_00000000001281996885_3317188729_init();
    work_m_00000000004147595947_2912294607_init();
    work_m_00000000002013452923_2073120511_init();


    xsi_register_tops("work_m_00000000004147595947_2912294607");
    xsi_register_tops("work_m_00000000002013452923_2073120511");


    return xsi_run_simulation(argc, argv);

}
