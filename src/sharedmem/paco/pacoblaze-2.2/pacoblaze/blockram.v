/*
	Copyright (C) 2004 Pablo Bleyer Kocik.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	this list of conditions and the following disclaimer in the documentation
	and/or other materials provided with the distribution.

	3. The name of the author may not be used to endorse or promote products
	derived from this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
	EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
	SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
	IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
	ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
	POSSIBILITY OF SUCH DAMAGE.
*/

/** @file
	BlockRAM model
*/

module blockram(
	clk, rst,
	enb, wen, addr, din, dout
);
parameter width = 8, depth = 6, size = 1<<depth;

input clk, rst, enb, wen;
input [depth-1:0] addr;
input [width-1:0] din;
output reg [width-1:0] dout;

reg [width-1:0] ram[0:size-1];

always @(posedge clk)
	if (rst) dout <= 'hx; // uninitialized
	else if (enb)
		if (wen) ram[addr] <= din;
		else dout <= ram[addr];

endmodule


module blockramDuo(
	clk, rst,
	enb1, wen1, addr1, din1, dout1,
	enb2, wen2, addr2, din2, dout2
);
parameter width = 8, depth = 6, size = 1<<depth;

input clk, rst;
input enb1, wen1;
input [depth-1:0] addr1;
input [width-1:0] din1;
output reg [width-1:0] dout1;

input enb2, wen2;
input [depth-1:0] addr2;
input [width-1:0] din2;
output reg [width-1:0] dout2;

reg [width-1:0] ram[0:size-1];

always @(posedge clk)
	if (rst) dout1 <= 'hx; // uninitialized
	else 
	begin 
	if (enb1)
		if (wen1) ram[addr1] <= din1;
		else dout1 <= ram[addr1];
	if (enb2)
		if (wen2) ram[addr2] <= din2;
		else dout2 <= ram[addr2];
	end
endmodule
