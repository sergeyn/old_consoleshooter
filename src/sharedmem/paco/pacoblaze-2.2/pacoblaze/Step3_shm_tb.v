
`define PACOBLAZE3M
`define HAS_DEBUG

`define TEST_FILE "/home/snepomny/VLSI/SW/readmem.rmh"
`include "timescale_inc.v"
`include "pacoblaze_inc.v"
`define SIM

module Step3_shm_tb;

parameter tck = 10, program_cycles = 102400;

reg clk;
reg rst, ir; // clock, reset, interrupt request
wire [`code_depth-1:0] ad; // instruction address
wire [`operand_width-1:0] pa, po; // port id, port output
wire rd, wr; // read strobe, write strobe
`ifdef HAS_INTERRUPT_ACK
wire ia; // interrupt acknowledge
`endif

reg [`operand_width-1:0] prt[0:`port_size-1]; // port memory

wire [`code_width-1:0] di; // program data input
wire [`operand_width-1:0] pi = prt[pa]; // port input
wire [`scratch_depth-1:0] ram_address1;
wire [`scratch_width-1:0] ram_write1;
wire [`scratch_width-1:0] ram_read1;
wire ram_write_en1;
wire ram_en1;

wire [`scratch_depth-1:0] ram_address2;
wire [`scratch_width-1:0] ram_write2;
wire [`scratch_width-1:0] ram_read2;
wire ram_write_en2;
wire ram_en2;
 
/* PacoBlaze program memory */
blockram #(.width(`code_width), .depth(`code_depth))
	rom(
	.clk(clk),
	.rst(rst),
	.enb(1'b1),
	.wen(1'b0),
	.addr(ad),
	.din(`code_width 'b0),
	.dout(di)
);


/* PacoBlaze ram */
memory_scratch scratch(
	.address1(ram_address1),
	.write_enable1(ram_write_en1), 
	.data_in1(ram_write1), 
	.data_out1(ram_read1),
	.address2(ram_address2),
	.write_enable2(ram_write_en2), 
	.data_in2(ram_write2), 
	.data_out2(ram_read2),
	.reset(1'b0),
	.clk(clk)
);

/* PacoBlaze dut */
pacoblaze3m dut(
	.clk(clk),
	.reset(rst),
	.address(ad),
	.instruction(di),
	.ram_address(ram_address1), 
	.ram_write(ram_write1), 
	.ram_read(ram_read1), 
	.ram_write_en(ram_write_en1), 
	.ram_en(ram_en1),
	.port_id(pa),
	.read_strobe(rd),
	.write_strobe(wr),
	.in_port(pi),
	.out_port(po),
	.interrupt(ir)
`ifdef HAS_INTERRUPT_ACK
	,	.interrupt_ack(ia)
`endif
);

`ifdef SIM
/* Clocking device */
always #(tck/2) clk = ~clk;

/* Watch port memory */
always @(posedge clk)	if (wr) prt[pa] <= po;

/* Simulation setup */
initial begin
	$dumpvars(0, dut);
	$dumpfile("pacoblaze3m_tb.vcd");
	$readmemh(`TEST_FILE, rom.ram);
`ifdef HAS_DEBUG
	$monitor("%s,\t odd regs: %x \t even regs: %x ", 
		dut.idu_debug, 
		dut.register.dpre,
		dut.register.dpro,
		);
	
`endif
end 

/* Simulation */
integer i;
initial begin
	
	for (i=0; i<`port_size; i=i+1) prt[i] = i; // initialize ports
	clk = 0; rst = 1; ir = 0;
	#(tck*2);
	@(negedge clk) rst = 0; // free processor

	#(program_cycles*tck+100) begin
		$finish;
		end
end
`endif
endmodule
