/*
 * The SEI Software Open Source License, Version 1.0
 *
 * Copyright (c) 2004, Solution Engineering, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 *
 * 2. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:
 *       "This product includes software developed by the
 *        Solution Engineering, Inc. (http://www.seisw.com/)."
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 *
 * 3. The name "Solution Engineering" must not be used to endorse or
 *    promote products derived from this software without prior
 *    written permission. For written permission, please contact
 *    admin@seisw.com.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL SOLUTION ENGINEERING, INC. OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 */

package  geom.demo;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @based_on Code by Dan Bridenbecker, Solution Engineering, Inc.
 */
public class PolyDemo
{   

   private JFrame       m_MainFrame ;
   private PolyCanvas   m_Canvas ;
   private ControlPanel m_ControlPanel ;
   
   /** Creates a new instance of PolyDemo */
   public PolyDemo()
   {
      m_MainFrame = createFrame();
   }
   
    /**
    *
    */
   public void show()
   {
      m_MainFrame.pack();
      
      Dimension screenDim = Toolkit.getDefaultToolkit ().getScreenSize(); 
      Rectangle winDim = m_MainFrame.getBounds();
      m_MainFrame.setLocation((screenDim.width - winDim.width) / 2, 
                              (screenDim.height - winDim.height) / 2);  
      
      m_MainFrame.setVisible( true );
   }
   
    /**
    *
    */
   private JFrame createFrame()
   {
 
      JFrame retFrame = new JFrame();
       retFrame.getContentPane().setLayout( new BorderLayout() );
      retFrame.setDefaultCloseOperation( JDialog.DISPOSE_ON_CLOSE );
      retFrame.addComponentListener( new FrameComponentListener() );
      
      
      m_Canvas       = new PolyCanvas();
      m_ControlPanel = new ControlPanel(m_Canvas);
      

      StatusBar bar = new StatusBar();
      m_Canvas.addStateChangedListener( bar );
      
      retFrame.getContentPane().add( m_Canvas,       BorderLayout.CENTER );
      retFrame.getContentPane().add( m_ControlPanel, BorderLayout.EAST );
      retFrame.getContentPane().add( bar,            BorderLayout.SOUTH );
      
      return retFrame ;
   }
   
   // ---------------------
   // --- Inner Classes ---
   // ---------------------
   /**
    * Limit resizing of main frame
    */
   private class FrameComponentListener extends ComponentAdapter
   {
      private static final int MIN_WIDTH = 500;
      private static final int MIN_HEIGHT = 550;
      
      public FrameComponentListener()
      {
      }
      
      public void componentResized(ComponentEvent e)
      {
         // handle resizing of frame
         int width = e.getComponent().getWidth();
         int height = e.getComponent().getHeight();
         
         if( width <= MIN_WIDTH )
         {
            width = MIN_WIDTH ;
         }
         if( height <= MIN_HEIGHT )
         {
            height = MIN_HEIGHT ;
         }
         e.getComponent().setSize(width, height);
      }
   }

   /**
    * Show information about what the user should do next.
    */
   private class StatusBar extends JPanel implements StateChangedListener
   {
      /**
	 * 
	 */
	private static final long serialVersionUID = 4111731050795721004L;
	private JLabel m_Message = new JLabel("");
      
      public StatusBar()
      {
         super( new FlowLayout(FlowLayout.LEFT) );
         add( m_Message );
      }
      
      public void stateChanged( CanvasState newState )
      {
         String msg = ".... ";
         if( (newState == CanvasState.ENTERING_POLY_1)  )
         {
           msg = "Pick the vertices of the polygon";
         }
         else
         {
           // msg = SELECT_OPERATION ;
         }
         m_Message.setText( msg );
      }
   }
   
   // --------------------
   // --- Main Program ---
   // --------------------

   /**
    * The main to the PolyDemo application
    */
   public static void main( String[] argv )
   {
      new PolyDemo().show();
   }
}
