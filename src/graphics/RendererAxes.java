package graphics;


public class RendererAxes extends AbsRenderer {

	public void translate(StatesCapsule sC, float x, float y, float z,
			int vec[]) {
		float r,s,t,u,v,w;
		
		float sina = sC.getSin_alpha();
		float sinb = sC.getSin_beta();
		float cosa = sC.getCos_alpha();
		float cosb = sC.getCos_beta();
		float zca = z * sC.getCos_alpha();
		float zsa = z * sC.getSin_alpha();
		
		r = x;
		s = y*cosa+zsa;
		t = -y*sina+zca;
		
		u = r*cosb+t*sinb;
		w = s; 
		v = -r*sinb + t*cosb;
		
		float Y = w * sC.getDist_veiwport_from_screen() / (v + sC.getDist_of_veiwport_from_origin());
		float X = u * sC.getDist_veiwport_from_screen() / (v + sC.getDist_of_veiwport_from_origin());
		
		if (! sC.isPerspective) {
			Y = w;
			X = u;
		}
			
		vec[0] = (int) UsefulFunctions.my_Round(X
				* sC.getScreen_to_max_crd_ratio() + sC.getMidx());
		vec[1] = (int) UsefulFunctions.my_Round(Y
				* sC.getScreen_to_max_crd_ratio() + sC.getMidy());
	}
	
	
	public void render(int height, int width, StatesCapsule sC)	{
		tmp_height = height;
		int vec[] = new int[2];
		screen = new int[height + 1][width + 1]; // virtual screen

		int red = (255 << 24) | (255 << 16) | (0 << 8) | 0;
		int green = (255 << 24) | (0 << 16) | (255 << 8) | 0;
		int blue = (255 << 24) | (0 << 16) | (0 << 8) | 255;

		// first we would like to translate the origins (x,y,z)==(0,0,0) to screen coordinates
		translate(sC, 0, 0, 0, vec);
		int originx = vec[0];
		int originy = vec[1];
		
		//W
		drawLine(green, originx, originy, originx, originy-300);

		//U
		translate(sC, 20, 0, 0, vec);
		drawLine(red, originx, originy, vec[0], vec[1]);

		//V
		translate(sC, 0, 0, 10, vec);
		drawLine(blue, originx, originy, vec[0], vec[1]);
	} 


	public void Putpixel(int x, int y, int base, float intens) {
		if (x < 0 || y < 0 || y >= tmp_height )
			return;
		try {
			screen[x][y] = base;
		} catch (Exception e) {
			// System.out.print('e');
		}
	}

	public void drawLine(int base, int xP, int yP, int xQ, int yQ) {
		int x = xP, y = yP, D = 0, HX = xQ - xP, HY = yQ - yP, c, M, xInc = 1, yInc = 1;
		if (HX < 0) {
			xInc = -1;
			HX = -HX;
		}
		if (HY < 0) {
			yInc = -1;
			HY = -HY;
		}
		if (HY <= HX) {
			c = 2 * HX;
			M = 2 * HY;
			for (;;) {
				Putpixel(x, y, base, 0.0f);
				if (x == xQ)
					break;
				x += xInc;
				D += M;
				if (D > HX) {
					y += yInc;
					D -= c;
				}
			}
		} else {
			c = 2 * HY;
			M = 2 * HX;
			for (;;) {
				Putpixel(x, y, base, 0.0f);
				if (y == yQ)
					break;
				y += yInc;
				D += M;
				if (D > HY) {
					x += xInc;
					D -= c;
				}
			}
		}
	}

}
