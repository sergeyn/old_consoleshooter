package graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class Demo extends JFrame {

	private static final long serialVersionUID = 1884376712381323520L;
	static Demo mainW;
	static DemoFrame frame; 

	public Demo() {
		super("Demo");
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent keyEvent) {
				handleKeyEvent(keyEvent);
			}
		});
		
		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				frame.setSize(mainW.getWidth(),mainW.getHeight());
				frame.repaint();
			}
		});

	}

	public void handleKeyEvent(KeyEvent keyEvent) {
		//System.out.print("e");
	}

	public static void main(String[] args) {
		mainW = new Demo();
		
		
		
		mainW.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0);
			}
		});

		
		JDesktopPane desktop = new JDesktopPane();
		desktop.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent keyEvent) {
				mainW.handleKeyEvent(keyEvent);
			}
		});

		frame =  new DemoFrame();
		frame.setSize(350,350);
	//	frame.setLocation(70,0);
		frame.setVisible(true);
		desktop.add(frame);
		//JInternalFrame controlsa = new ControlsPanel(frame);
		//controlsa.setLocation(10,30);
		//desktop.add(controlsa);
		mainW.add(desktop);
		mainW.setSize(800,600);
		mainW.setLocation(200,200);
		mainW.setVisible(true);

		//mainW.setJMenuBar(create_menu());
	
	}
	
	
	// Where the GUI is created:
	public static JMenuBar create_menu( ) {
		
		// Where the GUI is created:
		JMenuBar menuBar;
		JMenu menu;
		JMenuItem menuItem;
		JCheckBoxMenuItem cbMenuItem;

		// Create the menu bar.
		menuBar = new JMenuBar();

		// Build the first menu.
		menu = new JMenu("Rendering");
		//--------------------------------
		cbMenuItem = new JCheckBoxMenuItem("Draw axes");
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				frame.handleEvent('a'); 
			//	mainW.repaint();
			}
		});
		menu.add(cbMenuItem);
		//--------------------------------
		cbMenuItem = new JCheckBoxMenuItem("Shade");
	//	frame.isShade = cbMenuItem;
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (frame.sC.getShade() == false) {
					if (frame.dx == null) // if no partial derivative
						frame.handleFunctionsDX();
					if (frame.dx == null) {// if still no valid derivative
						frame.isShade.setSelected(false);
						return;
					}

					if (frame.dz == null) // if no partial derivative
						frame.handleFunctionsDZ();
					if (frame.dz == null) {// if still no valid derivative
						frame.isShade.setSelected(false);
						return;
					}
				}

				frame.sC.setShade(!frame.sC.getShade());
				frame.renderer = DemoFrame.createRenderer(frame.sC, frame.get_width(), frame.get_height(), frame.f, frame.dx, frame.dz);
				frame.repaint();
			}
		});
		menu.add(cbMenuItem);
		//--------------------------------
		cbMenuItem = new JCheckBoxMenuItem("Normalize");
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				frame.sC.setNorm(!frame.sC.getNorm());
				frame.renderer = DemoFrame.createRenderer(frame.sC, frame.get_width(), frame.get_height(), frame.f, frame.dx, frame.dz);
				frame.repaint();
			}
		});
		//menu.add(cbMenuItem);
		//--------------------------------
		cbMenuItem = new JCheckBoxMenuItem("Orthogonal projection");
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				frame.sC.isPerspective = !frame.sC.isPerspective;
				frame.renderer = DemoFrame.createRenderer(frame.sC, frame.get_width(), frame.get_height(), frame.f, frame.dx, frame.dz);
				frame.repaint();
			}
		});
		//frame.isPerspective = cbMenuItem;
		menu.add(cbMenuItem);
		
		
		
		cbMenuItem = new JCheckBoxMenuItem("Ridge");
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				frame.sC.setRidge(!frame.sC.getRidge());
				frame.renderer = DemoFrame.createRenderer(frame.sC, frame.get_width(), frame.get_height(), frame.f, frame.dx, frame.dz);
				frame.repaint();
			}
		});
		//menu.add(cbMenuItem);

		menuBar.add(menu);

		menu = new JMenu("Functions");
		menuItem = new JMenuItem("Get function");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.handleFunction();
			}
		});
		menu.add(menuItem);

		menuItem = new JMenuItem("Get dx");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.handleFunctionsDX();
			}
		});
		menu.add(menuItem);

		menuItem = new JMenuItem("Get dy");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.handleFunctionsDZ();
			}
		});
		menu.add(menuItem);

		menuItem = new JMenuItem("Choose predefined function");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.handleFunctionsPredef();
			}
		});
		menu.add(menuItem);

		menuBar.add(menu);
		
		//========================================
		menu = new JMenu("Help");
		menuItem = new JMenuItem("Keyboard controls");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "<html>�X� and �x� � used for rotating the scene round the named axis <br>�Y� and �y� � used for rotating the scene round the named axis<br>�E� and �e� � narrowing and widening the positive and negative planes<br>�+� and �-� � controlling the distance of screen from origin<br>�D� and �d� � increasing and decreasing the distance<br>�P� and �p� � increasing and decreasing the distance of viewport from screen<br>�L� and �l� � increasing and decreasing intensiveness of  the light source<br></html>");
			}
		});
		menu.add(menuItem);

		
		menu.add(menuItem);

		//menuBar.add(menu);
		//========================================
		return (menuBar);
	}
	
}
