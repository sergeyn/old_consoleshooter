package graphics;

/**
 * Stub implementation of the CRenderer interface
 */
public abstract class AbsRenderer implements CRenderer {
	// Holds the pixles of the screen
	protected int screen[][];
	protected int tmp_height;

	// Dummy PutPixel
	public void Putpixel(int x, int y, int base, float intens) {
		if (x < 0 || y < 0 || y >= tmp_height )
			return;
		
		int val = 55;
		try {
			screen[x][y] = (255 << 24) | (val << 16) | (val << 8) | val;
		} catch (Exception e) {
		}
	}

	// Getter for the scereen matrix
	public int[][] getScreen() {
		return screen;
	}

	// "renders" blank image
	public void render(int height, int width) {
		screen = new int[height + 1][width + 1];
	}

	/**
	 * Simple line drawing function
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	protected void Line(int x1, int y1, int x2, int y2) {
		int i, y3 = (y1 + y2) / 2;
		if (y1 < y2) {
			for (i = y1 + 1; i <= y3; i++)
				Putpixel(x1, i, 0, 0.0f);
			for (i = y3 + 1; i <= y2; i++)
				Putpixel(x2, i, 0, 0.0f);
		} else if (y1 > y2) {
			for (i = y1 - 1; i > y3; i--)
				Putpixel(x1, i, 0, 0.0f);
			for (i = y3; i >= y2; i--)
				Putpixel(x2, i, 0, 0.0f);
		} else
			Putpixel(x2, y2, 1, 0.0f);
	} // end of Line.
}
