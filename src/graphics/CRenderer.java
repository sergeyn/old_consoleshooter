package graphics;

/**
 * Renderers interface - defines the most basic methods for rendering on top of
 * abstract screen
 */
public interface CRenderer {
	/**
	 * Creates the image of desired dimensions given the states machine for the
	 * environment
	 * 
	 * @param height
	 *            height of image
	 * @param width
	 *            width of image
	 * @param sC
	 *            the states machine for the environment
	 */
	public void render(int height, int width, StatesCapsule sC);

	/**
	 * Getter for the rendered picture as a matrix of ints
	 * 
	 * @return
	 */
	public int[][] getScreen();

	/**
	 * This method is used to put pixels on top of abstract screen
	 * 
	 * @param x
	 *            coordinate of the pixel
	 * @param y
	 *            coordinate of the pixel
	 * @param base
	 *            color
	 * @param intens
	 *            intensity [when matters]
	 */
	public void Putpixel(int x, int y, int base, float intens);
}
