package graphics;

public class StatesCapsule {

	boolean isPerspective;
	boolean Shade;
	boolean Ridge;
	boolean Norm;

	final public static float Angle_step = 0.25f; // increment of turning
													// angle
	final public static float Min_angle = -1.5f;
	final public static float Max_angle = 1.5f;

	float angle_Alpha, angle_Beta; // initial turning angles
	float sin_alpha, cos_alpha, sin_beta, cos_beta; // sin & cos of Alpha and
													// Beta
	float tana_alpha, cosa_x_sinb, cosa_x_cosb, sina_x_sinb, sina_x_cosb;
	float screen_to_max_crd_ratio; // ratio of screen size over max coordinate
									// values
	int Maxx, Maxy; // maximum coordinates of x and y
	int Midx, Midy; // coordinates of screen middle
	float Dist;

	float dist_veiwport_from_screen; // dist of viewpoint from screen
	float dist_of_screen_from_origin; // dist of screen from origin
	float original_distance_of_screen_from_origin; // Original distance of
													// screen from origin
	float dist_of_veiwport_from_origin; // dist of viewpoint from origin (D1+D2)
	float neg_t_plane_limit, pos_t_plane_limit; // limiting values for t-plane
	float RADIUS;
	float T_STEP;
	float FUN_MAX;
	// intense specific
	float Thldn; // Threshold value for LdotN - see Intensity()
	float Thin; // Threshold for intensity
	float AMBIENT = 0.25f;
	float Ip = 1.0f - AMBIENT;

	private void calcTrig() {
		sin_alpha = (float) Math.sin(angle_Alpha);
		cos_alpha = (float) Math.cos(angle_Alpha);
		sin_beta = (float) Math.sin(angle_Beta);
		cos_beta = (float) Math.cos(angle_Beta);

		tana_alpha = (float) Math.tan(angle_Alpha);
		cosa_x_sinb = cos_alpha * sin_beta;
		cosa_x_cosb = cos_alpha * cos_beta;
		sina_x_sinb = sin_alpha * sin_beta;
		sina_x_cosb = sin_alpha * cos_beta;
	}

	StatesCapsule(int height, int width) {

		RADIUS = 5.0f;
		T_STEP = 0.5f;
		FUN_MAX = 3.0f;

		Maxx = height - 1;
		Maxy = width - 1;
		Midx = Maxx / 2;
		Midy = Maxy / 2;
		screen_to_max_crd_ratio = Midy / RADIUS; // ratio of screen height
													// (in pixels) to RADIUS
		angle_Alpha = 0.0f; 
		angle_Beta = 0.0f;
		calcTrig();

		dist_veiwport_from_screen = 32.0f;
		dist_of_screen_from_origin = 4.0f;
		original_distance_of_screen_from_origin = 4.0f;
		dist_of_veiwport_from_origin = dist_veiwport_from_screen
				+ dist_of_screen_from_origin;

		Dist = 1.0f;
		neg_t_plane_limit = -3.25f;
		pos_t_plane_limit = 3.25f;

		Shade = false;
		Ridge = false;
		Norm = false;
		isPerspective = true;
	}

	public float getAngle_Alpha() {
		return angle_Alpha;
	}

	public float getAngle_Beta() {
		return angle_Beta;
	}

	public float getSin_alpha() {
		return sin_alpha;
	}

	public float getCos_alpha() {
		return cos_alpha;
	}

	public float getSin_beta() {
		return sin_beta;
	}

	public float getCos_beta() {
		return cos_beta;
	}

	public float getTana_alpha() {
		return tana_alpha;
	}

	public float getCosa_x_sinb() {
		return cosa_x_sinb;
	}

	public float getCosa_x_cosb() {
		return cosa_x_cosb;
	}

	public float getSina_x_sinb() {
		return sina_x_sinb;
	}

	public float getSina_x_cosb() {
		return sina_x_cosb;
	}

	public float getScreen_to_max_crd_ratio() {
		return screen_to_max_crd_ratio;
	}

	public int getMaxx() {
		return Maxx;
	}

	public int getMaxy() {
		return Maxy;
	}

	public int getMidx() {
		return Midx;
	}

	public int getMidy() {
		return Midy;
	}

	public float getDist() {
		return Dist;
	}

	public float getDist_veiwport_from_screen() {
		return dist_veiwport_from_screen;
	}

	public float getDist_of_screen_from_origin() {
		return dist_of_screen_from_origin;
	}

	public float getOriginal_distance_of_screen_from_origin() {
		return original_distance_of_screen_from_origin;
	}

	public float getDist_of_veiwport_from_origin() {
		return dist_of_veiwport_from_origin;
	}

	public float getNeg_t_plane_limit() {
		return neg_t_plane_limit;
	}

	public float getPos_t_plane_limit() {
		return pos_t_plane_limit;
	}

	public float getRADIUS() {
		return RADIUS;
	}

	public float getT_STEP() {
		return T_STEP;
	}

	public float getFUN_MAX() {
		return FUN_MAX;
	}

	public void setAngle_Alpha(float angle_Alpha) {
		this.angle_Alpha = Math
				.min(Max_angle, Math.max(angle_Alpha, Min_angle));
		calcTrig();
	}

	public void setAngle_Beta(float angle_Beta) {
		this.angle_Beta = Math.min(Max_angle, Math.max(angle_Beta, Min_angle));
		calcTrig();
	}

	public void setMaxx(int maxx) {
		Maxx = maxx;
		Midx = Maxx / 2;
	}

	public void setMaxy(int maxy) {
		Maxy = maxy;
		Midy = Maxy / 2;
		screen_to_max_crd_ratio = Midy / RADIUS;
	}

	public void setDist(float dist) {
		Dist = Math.max(0.0f, dist);
	}

	public void setDist_veiwport_from_screen(float dist_veiwport_from_screen) {

		this.dist_veiwport_from_screen = Math.max(1.0f,
				dist_veiwport_from_screen);
		setDist_of_veiwport_from_origin(); // D3=D1+D2
	}

	public void setDist_of_screen_from_origin(float dist_of_screen_from_origin) {
		this.dist_of_screen_from_origin = dist_of_screen_from_origin;
		setDist_of_veiwport_from_origin(); // D3=D1+D2
	}

	public void setOriginal_distance_of_screen_from_origin(
			float original_distance_of_screen_from_origin) {
		this.original_distance_of_screen_from_origin = original_distance_of_screen_from_origin;
	}

	private void setDist_of_veiwport_from_origin() {
		this.dist_of_veiwport_from_origin = this.getDist_veiwport_from_screen()
				+ this.getDist_of_screen_from_origin();
	}

	public void setNeg_t_plane_limit(float neg_t_plane_limit) {
		this.neg_t_plane_limit = neg_t_plane_limit;
	}

	public void setPos_t_plane_limit(float pos_t_plane_limit) {
		this.pos_t_plane_limit = pos_t_plane_limit;
	}

	public void setRADIUS(float radius) {
		RADIUS = radius;
	}

	public void setT_STEP(float t_step) {
		T_STEP = t_step;
	}

	public void setFUN_MAX(float fun_max) {
		FUN_MAX = fun_max;
	}

	public boolean getShade() {
		return Shade;
	}

	public void setShade(boolean shade) {
		Shade = shade;
	}

	public boolean getRidge() {
		return Ridge;
	}

	public void setRidge(boolean ridge) {
		Ridge = ridge;
	}

	public boolean getNorm() {
		return Norm;
	}

	public void setNorm(boolean norm) {
		Norm = norm;
	}

	public float getThldn() {
		return Thldn;
	}

	public void setThldn(float thldn) {
		Thldn = thldn;
	}

	public float getThin() {
		return Thin;
	}

	public void setThin(float thin) {
		Thin = thin;
	}

	public float getAMBIENT() {
		return AMBIENT;
	}

	public void setAMBIENT(float ambient) {
		AMBIENT = ambient;
		Ip = 1.0f - AMBIENT;
	}

	public float getIp() {
		return Ip;
	}

	public void setIp(float ip) {
		Ip = ip;
	}

}
