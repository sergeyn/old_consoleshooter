package graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.javia.arity.Function;
import org.javia.arity.Symbols;
import org.javia.arity.SyntaxException;

public class FloatingColumnMain extends JFrame {

	private static final long serialVersionUID = -8089591596801718261L;
	private final static String default_functionCode = "3.0*sin(x*cos(y))";
	private final static String default_dxCode = "(3.*cos(x*cos(y))*cos(y))";
	private final static String default_dzCode = "(-3.*cos(x*cos(y))*x*sin(y))";
	
	private final static String predefined_functions[] = { 
		"-3 * cos(x) ",
		"3 * sin(x) * cos(y)",
		"3 * sin(x)^2 * cos(y)",
		"3 * sin(x^2) * cos(y)^2",
		"sin( x^2 + y^2 )", 
		"sin( x + y^2 )",
		"3 * sin( x * cos(y) )",
		"x * y^2" };
	private final static String predefined_dx[] = {
		"3.*sin(x)",
		"3.*cos(x)*cos(y)",
		"6.*sin(x)*cos(x)*cos(y)",
		"6.*cos(x*x)*x*cos(y)*cos(y)",
		"2.*x*cos(x*x+y*y)",
		"cos(x+y*y)",
		"3.*cos(x*cos(y))*cos(y)",
		"y^2" };
	private final static String predefined_dy[] = { 
		"0",
		"-3.*sin(x)*sin(y)",
		"-3.*sin(x)*sin(x)*sin(y)",
		"-6.*sin(x*x)*cos(y)*sin(y)",
		"2.*y*cos(x*x+y*y)", 
		"2.*y*cos(x+y*y)",
		"-3.*cos(x*cos(y))*x*sin(y)",
		"2*x*y" };	

	

	public static FloatingColumnMain mainW;

	JCheckBoxMenuItem isShade;
	private StatesCapsule sC;
	private int _width = 400;
	private int _height = 400;

	private CRenderer renderer;
	private CRenderer second_render;
	private ImagePanel image_panel;
	private Function f;
	private Function dx = null;
	private Function dz = null;
    private boolean axesFlag = false;
    
	FloatingColumnMain() {
	
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0);
			}
		});

		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent keyEvent) {
				mainW.handleKeyEvent(keyEvent);
			}
		});
		addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				repaint();
			}
		});
		
	}

	public static void main1(String[] args) {
		mainW = new FloatingColumnMain();

		try { // initialize function and partial derivatives
			Symbols symbols = new Symbols();
			mainW.setF(symbols.compile(default_functionCode));
			mainW.setDx(symbols.compile(default_dxCode));
			mainW.setDz(symbols.compile(default_dzCode));
		} catch (SyntaxException e) {
			return;
		}

		// create new state machine
		mainW.setSC(new StatesCapsule(mainW.get_height(), mainW.get_width()));
		mainW.create_menu();

		// create default main renderer
		mainW.setRenderer(new RendererFlat(mainW.get_height(), mainW
				.get_width(), mainW.getF()));
		// create second_renderer - in this cases - Axes renderer
		mainW.second_render = new RendererAxes(); //mainW.get_height(), mainW 				.get_width(), mainW.getF());

		// render you first image
		mainW.renderer.render(mainW.get_width(), mainW.get_height(), mainW
				.getSC());
		// put it on panel
		mainW.image_panel = new ImagePanel(
				mainW.renderer.getScreen() ,
				mainW.get_width(), mainW.get_height());
		
		//mainW.setLayout(null);
		mainW.image_panel.setLocation(0, 60);
		
		mainW.setSize(mainW.get_height() + 100, mainW.get_width() + 100);
		mainW.setLocation(200, 200);
		
		JDesktopPane desktop = new JDesktopPane();
		JInternalFrame f =  new JInternalFrame("Internal Frame", true, true, true, true );
		f.add(mainW.image_panel);
		f.setSize(100,100);
		f.setVisible(true);
		desktop.add(f);
		mainW.add(desktop);
		mainW.setVisible(true);
	}

	/**
	 * Used to issue 'repaint' order: 1) Will cause the default (and perhaps
	 * secondary) renderer to render 2) Will update the target image panel
	 */
	public void repaint() {
		int _size = Math.min(this.getHeight() - 100, this.getWidth() - 100);
		sC.setMaxx(_size - 1);
		sC.setMaxy(_size - 1);
		renderer.render(_size, _size, sC);
		second_render.render(_size, _size, mainW.getSC());
		image_panel.init(renderer.getScreen(), _size, _size);
		if (axesFlag)
			image_panel.overdraw(second_render.getScreen(), _size, _size);
		image_panel.repaint();
	}

	private void afterFReset(Function f, boolean isPredef) {
		this.setF(f);
		this.isShade.setSelected(false);
		this.sC.setShade(false);
		
		if (! isPredef) {
			this.setDx(null);
			this.setDz(null);
			// NOTE: The derivatives must be set manually in current version of the
			// code!
		}
		this.setRenderer(new RendererFlat(this._height, this._width, f));
		this.repaint();
	}

	/**
	 * Handles function changing via GUI
	 */
	private void handleFunction() {
		Function f = handleFunctionsAll("Please enter your function:\n", null)[0];
		if (f == null)
			return;
		afterFReset(f,false);

	}

	/**
	 * Handles choosing one of the predefined functions
	 */
	private void handleFunctionsPredef() {
		Function[] f_arr = handleFunctionsAll("Please choose your function:\n",
				predefined_functions);
		if (null == f_arr) 
			return;
		f = f_arr[0];
		dx = f_arr[1]; dz = f_arr[2];
		afterFReset(f,true);
	}

	/**
	 * Handle partial derivative input from user
	 * 
	 * @see handleFunctionsAll
	 */
	private void handleFunctionsDZ() {
		dz = handleFunctionsAll("Please enter your function (DY):\n", null)[0];
	}

	/**
	 * Handle partial derivative input from user
	 * 
	 * @see handleFunctionsAll
	 */
	private void handleFunctionsDX() {
		dx = handleFunctionsAll("Please enter your function (DX):\n", null)[0];
	}

	/**
	 * Generic function for handling function input from user
	 * 
	 * @param text
	 *            The dipslay string for the input box
	 * @param options
	 *            Optional field (may be null) stating predefined functions
	 * @return Constructed function
	 */
	private Function[] handleFunctionsAll(String text, Object[] options) {
		String fs = (String) JOptionPane.showInputDialog(this, text,
				"Functions Dialog", JOptionPane.PLAIN_MESSAGE, null, options,
				"");
		if (fs == null)
			return null;
		Function f = null;
		Function[] results = new Function[3];
		try {
			// compile new functions from string representation
			f = (new Symbols().compile(fs + "+0x+0y")); // "+0x+0y" is to match
														// arity of function to
														// 2
			if (options != null) { //if we are in predefined domain 
				int index = getIndexOfPredefinedFunction(fs);
				results[1] = (new Symbols().compile(predefined_dx[index] + "+0x+0y"));
				results[2] = (new Symbols().compile(predefined_dy[index] + "+0x+0y"));
			}
		} catch (SyntaxException e) {
			f = null;
			JOptionPane.showMessageDialog(null,
					"Failed setting function - check your input");
		}
		results[0] = f;
		sC = new StatesCapsule(mainW.get_height(), mainW.get_width()); 
		return results;
	}
	
	private int getIndexOfPredefinedFunction(String fs) {
		for (int i=0; i < predefined_functions.length; ++i) 
			if (fs.equals( predefined_functions[i]) )
				return i;
		return -1;
	}

	/**
	 * Keyboard event handler
	 * 
	 * @param keyEvent
	 *            structure holding keyboard event data
	 */
	private void handleKeyEvent(KeyEvent keyEvent) {
		switch (keyEvent.getKeyChar()) {
		// case 's' : sC.setShade(!sC.getShade()); break;
		// case 'n' : sC.setNorm(!sC.getNorm()); break;
		// case 'r' : sC.setRidge(!sC.getRidge()); break;
		case 'e':
			sC.setNeg_t_plane_limit(sC.getNeg_t_plane_limit()-1.0f);
			sC.setPos_t_plane_limit(sC.getPos_t_plane_limit()+1.0f);
			break;
		case 'E':
			sC.setNeg_t_plane_limit(sC.getNeg_t_plane_limit()+1.0f);
			sC.setPos_t_plane_limit(sC.getPos_t_plane_limit()-1.0f);
			break;
		case '+':
			sC.setDist_of_screen_from_origin(2 * sC
					.getDist_of_screen_from_origin());
			break;
		case '-':
			sC
					.setDist_of_screen_from_origin(sC
							.getDist_of_screen_from_origin() / 2);
			break;
		case 'D':
			sC.setDist(sC.getDist() + 0.25f);
			break;
		case 'd':
			sC.setDist(sC.getDist() - 0.25f);
			break; // Dist -= 0.25;
		case 'p':
			sC.setDist_veiwport_from_screen(sC.getDist_veiwport_from_screen() / 2);
			break;
		case 'P':
			sC.setDist_veiwport_from_screen(2 * sC
					.getDist_veiwport_from_screen());
			break;
		case 'k':
			sC
					.setDist_veiwport_from_screen(sC
							.getDist_veiwport_from_screen() / 2);
			break;
		case 'K':
			sC
					.setDist_veiwport_from_screen(sC
							.getDist_veiwport_from_screen() * 2);
			break;
		case 'x':
			sC.setAngle_Alpha(sC.getAngle_Alpha() + sC.Angle_step);
			break;
		case 'X':
			sC.setAngle_Alpha(sC.getAngle_Alpha() - sC.Angle_step);
			break;
		case 'Y':
			sC.setAngle_Beta(sC.getAngle_Beta() + sC.Angle_step);
			break;
		case 'y':
			sC.setAngle_Beta(sC.getAngle_Beta() - sC.Angle_step);
			break;
		case 'L':
			sC.setIp(sC.getIp() + 0.125f);
			break;
		case 'l':
			sC.setIp(sC.getIp() - 0.125f);
			break;
		case 'T':
			sC.setThldn(sC.getThldn() * 2.0f);
			break;
		case 't':
			sC.setThldn(sC.getThldn() * 0.5f);
			break;
		case 'b':
			mainW.setSC(new StatesCapsule(mainW.get_height(), mainW.get_width()));
			break;			
		case 'I':
			sC.setThin(sC.getThin() * 2.0f);
			break;
		case 'i':
			sC.setThin(sC.getThin() * 2.0f);
			break;
		}
		;
		repaint();
	}

	/**
	 * Static factory for renderers
	 * 
	 * @param sC
	 *            state machine encapsulating all environment variables
	 * @param _w
	 *            preferred width
	 * @param _h
	 *            preferred height
	 * @param f
	 *            default function
	 * @param dx
	 *            partial derivative_x
	 * @param dz
	 *            partial derivative_z
	 * @return constructed renderer object
	 */
	public static CRenderer createRenderer(StatesCapsule sC, int _w, int _h,
			Function f, Function dx, Function dz) {
		if (sC.getShade()) // if shading mode is on:
			if (sC.getNorm()) // either we use normals from dx,dz
				return new RendererNormal(_w, _h, f, dx, dz);
			else
				// or we do intens. interpolation
				return new RendererIntense(_w, _h, f, dx, dz);
		else
			// the third choice is to render flat and in monochrome
			return new RendererFlat(_w, _h, f);
	}

	public StatesCapsule getSC() {
		return sC;
	}

	public void setSC(StatesCapsule sc) {
		sC = sc;
	}

	public int get_width() {
		return _width;
	}

	public void set_width(int _width) {
		this._width = _width;
	}

	public int get_height() {
		return _height;
	}

	public void set_height(int _height) {
		this._height = _height;
	}

	public FloatingColumnMain getMainW() {
		return mainW;
	}

	public CRenderer getRenderer() {
		return renderer;
	}

	public void setRenderer(CRenderer rf) {
		this.renderer = rf;
	}

	public ImagePanel getImage_panel() {
		return image_panel;
	}

	public void setImage_panel(ImagePanel ip) {
		this.image_panel = ip;
	}

	public Function getF() {
		return f;
	}

	public void setF(Function f) {
		this.f = f;
	}

	public Function getDx() {
		return dx;
	}

	public void setDx(Function dx) {
		this.dx = dx;
	}

	public Function getDz() {
		return dz;
	}

	public void setDz(Function dz) {
		this.dz = dz;
	}

	// Where the GUI is created:
	public void create_menu() {
		
		// Where the GUI is created:
		JMenuBar menuBar;
		JMenu menu;
		JMenuItem menuItem;
		JCheckBoxMenuItem cbMenuItem;

		// Create the menu bar.
		menuBar = new JMenuBar();

		// Build the first menu.
		menu = new JMenu("Rendering");
		//--------------------------------
		cbMenuItem = new JCheckBoxMenuItem("Draw axes");
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				axesFlag = !axesFlag;
				repaint();
			}
		});
		menu.add(cbMenuItem);
		//--------------------------------
		cbMenuItem = new JCheckBoxMenuItem("Shade");
		isShade = cbMenuItem;
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if (sC.getShade() == false) {
					if (dx == null) // if no partial derivative
						handleFunctionsDX();
					if (dx == null) {// if still no valid derivative
						isShade.setSelected(false);
						return;
					}

					if (dz == null) // if no partial derivative
						handleFunctionsDZ();
					if (dz == null) {// if still no valid derivative
						isShade.setSelected(false);
						return;
					}
				}

				sC.setShade(!sC.getShade());
				renderer = createRenderer(sC, _width, _height, f, dx, dz);
				repaint();
			}
		});
		menu.add(cbMenuItem);
		//--------------------------------
		cbMenuItem = new JCheckBoxMenuItem("Normalize");
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				sC.setNorm(!sC.getNorm());
				renderer = createRenderer(sC, _width, _height, f, dx, dz);
				repaint();
			}
		});
		menu.add(cbMenuItem);
		cbMenuItem = new JCheckBoxMenuItem("Ridge");
		cbMenuItem.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				sC.setRidge(!sC.getRidge());
				renderer = createRenderer(sC, _width, _height, f, dx, dz);
				repaint();
			}
		});
		menu.add(cbMenuItem);

		menuBar.add(menu);

		menu = new JMenu("Functions");
		menuItem = new JMenuItem("Get function");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				handleFunction();
			}
		});
		menu.add(menuItem);

		menuItem = new JMenuItem("Get dx");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				handleFunctionsDX();
			}
		});
		menu.add(menuItem);

		menuItem = new JMenuItem("Get dy");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				handleFunctionsDZ();
			}
		});
		menu.add(menuItem);

		menuItem = new JMenuItem("Choose predefined function");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				handleFunctionsPredef();
			}
		});
		menu.add(menuItem);

		menuBar.add(menu);
		
		//========================================
		menu = new JMenu("Help");
		menuItem = new JMenuItem("Keyboard controls");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "<html>�X� and �x� � used for rotating the scene round the named axis <br>�Y� and �y� � used for rotating the scene round the named axis<br>�E� and �e� � narrowing and widening the positive and negative planes<br>�+� and �-� � controlling the distance of screen from origin<br>�D� and �d� � increasing and decreasing the distance<br>�P� and �p� � increasing and decreasing the distance of viewport from screen<br>�L� and �l� � increasing and decreasing intensiveness of  the light source<br></html>");
			}
		});
		menu.add(menuItem);

		
		menu.add(menuItem);

		menuBar.add(menu);
		//========================================
		this.setJMenuBar(menuBar);
	}
}
