package graphics;

public class UsefulFunctions {

	public static class Intrpl {
		float x0, y0, lin;

		public void init(float x0, float y0, float x1, float y1) {
			this.x0 = x0;
			if (Math.abs(x0 - x1) < 1.e-5) {
				this.y0 = 0.5f * (y0 + y1);
				this.lin = 0.0f;
			} else {
				this.y0 = y0;
				this.lin = (y1 - y0) / (x1 - x0);
			}
		}

		float InterValue(float x) {
			return (y0 + lin * (x - x0));
		}
	}

	public static class my_Vector3d {
		float vx;
		float vy;
		float vz;

		my_Vector3d() {
			vx = vy = vz = 0.0f;
		}

		my_Vector3d(float X, float Y, float Z) {
			vx = X;
			vy = Y;
			vz = Z;
		}

		float Length() {
			return (float) Math.sqrt(vx * vx + vy * vy + vz * vz);
		}

		public void Normalize() {

			float len = Length();
			if (len > 0.0f) {
				vx /= len;
				vy /= len;
				vz /= len;
			}
		}

		float Dot(my_Vector3d pv) {
			return (vx) * (pv.vx) + (vy) * (pv.vy) + (vz) * (pv.vz);
		}

		my_Vector3d Cross(my_Vector3d pv) {
			my_Vector3d s = new my_Vector3d();
			s.vx = (vy) * (pv.vz) - (pv.vy) * (vz);
			s.vy = -(vx) * (pv.vz) + (pv.vx) * (vz);
			s.vz = (vx) * (pv.vy) - (pv.vx) * (vy);
			return s;
		}
	}

	public static class FHR {
		public int ymin;
		public int ymax;
		public int yprev;

		public FHR() {
			// with those we are going to be searching for max and min - hence
			// the values
			ymin = Integer.MAX_VALUE; // Initialize min value in column.
			ymax = Integer.MIN_VALUE; // Initialize max value in column.
		}
	}

	public static float my_Round(float x) {
		// ROUND(x) (floor((x)+0.5))
		return (float) Math.floor(x + 0.5f);
	}

	public static float SQ(float x) {
		return x * x;
	}
}
