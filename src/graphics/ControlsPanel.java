package graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ControlsPanel extends JPanel {
	private  DemoFrame frame;
	public static final int btsz = 50;
	public void act(JButton b, final char my_action) {
		b.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent arg0) {
			frame.handleEvent(my_action);
			}});
	}
	
	public void set( String caption, final char action, int left, int top) {
		JButton bt;

		bt = new JButton(caption);
		bt.setLocation(left, top);
		int factor = caption.length() > 1 ? 2 : 1;
		bt.setSize(factor*btsz,20);
		
		act(bt,action);
		add(bt);
	}
	
	public void set( String caption, int left, int top) {
		JLabel bt;

		bt = new JLabel(caption);
		bt.setLocation(left, top);
		bt.setSize(100,20);
		add(bt);
	}
	
	public ControlsPanel(DemoFrame delegate) {
		//super("Controls",false, false, false, false);
		frame = delegate;
		setVisible(true);
	}
	
	
}
